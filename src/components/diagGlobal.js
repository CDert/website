import React, {useRef, useEffect} from 'react';
import Diagram, {
  Nodes,
  Edges,
  AutoLayout,
  Toolbox,
  Group,
  MainToolbar,
  ViewToolbar,
  HistoryToolbar,
  PropertiesPanel,
  Editing,
  ContextToolbox
} from 'devextreme-react/diagram';
import ArrayStore from 'devextreme/data/array_store';
import {Helmet} from "react-helmet";

const Diag = () => {
  const flowNodes = [
    {
      id: 1,
      text: 'Transiscope en Pays Nantais',
      left: 0,
      width: 3,
      top: 0,
      style: "fill: #d9d9d9; stroke: #999999",
      kind: "collectif"
    }, {
      id: 2,
      text: 'Livre Blanc pour le climat',
      left: 5,
      top: 0,
      width: 3,
      kind: "collectif"
    }, {
      id: 10,
      text: 'source pour Transiscope',
      left: 1,
      top: 5,
      kind: "chantier"
    }, {
      id: 11,
      text: `L'autre Agenda Nantais`,
      left: 1,
      top: 3,
      kind: "chantier"
    }, {
      id: 12,
      text: `Interconnexions des alternatives en pays nantais`,
      url: './ian',
      left: 1,
      top: 1,
      kind: "chantier"
    }, {
      id: 13,
      text: 'Livre Blanc',
      left: 6,
      top: 5,
      kind: "chantier"
    }, {
      id: 14,
      text: 'Ateliers Citoyens',
      left: 6,
      top: 1,
      kind: "chantier"
    }, {
      id: 15,
      text: 'Barometre',
      left: 6,
      top: 3,
      kind: "chantier"
    }, {
      id: 50,
      top: 1,
      text: `"Interconnexions et Climat"\naccompagné par \n"Territoires D'expérimentations"`,
      url: './Campagne',
      left: 2.5,
      height: 2,
      width: 3,
      type: "ellipse",
      kind: "campagne"
    }, {
      id: 51,
      top: 4,
      text: `autres collectifs`,
      left: 3.5,
      height: 1,
      width: 1,
      kind: "collectif"
    }
  ];
  const flowEdges = [
    {
      id: 101,
      fromId: 1,
      toId: 10,
      toPointIndex: 3
    }, {
      id: 102,
      fromId: 1,
      toId: 11,
      toPointIndex: 3
    }, {
      id: 103,
      fromId: 1,
      toId: 12,
      toPointIndex: 3
    }, {
      id: 104,
      fromId: 2,
      toId: 13,
      toPointIndex: 1
    }, {
      id: 104,
      fromId: 2,
      toId: 14,
      toPointIndex: 1
    }, {
      id: 105,
      fromId: 2,
      toId: 15,
      toPointIndex: 1
    }, {
      id: 106,
      fromId: 12,
      toId: 50
    }, {
      id: 107,
      fromId: 14,
      toId: 50
    }, {
      id: 108,
      fromId: 15,
      toId: 50
    }, {
      id: 109,
      fromId: 1,
      toId: 50
    }, {
      id: 110,
      fromId: 2,
      toId: 50
    }, {
      id: 111,
      fromId: 51,
      toId: 50
    }
  ];

  const flowNodesDataSource = new ArrayStore({key: 'id', data: flowNodes});
  const flowEdgesDataSource = new ArrayStore({key: 'id', data: flowEdges});

  const style = (el) => {
    let style = {};
    switch (el.kind) {
      case "collectif":
        style = {
          ...style,
          fill: "mediumseagreen"
        }
        break;
      case "chantier":
        style = {
          ...style,
          fill: "silver"
        }
        break;
      case "campagne":
        style = {
          ...style,
          fill: "teal"
        }
        break;
      default:

    }
    // console.log('style',style);
    return style;
  }

  const styleText = (el) => {
    // console.log('ALLLLO');
    let style = {};
    switch (el.kind) {
      case "campagne":
        style = {
          ...style,
          "fill": "white"
        }
        break;
      default:
    }
    return style;
  }

  const navigate = (el) => {
    if (el.item.dataItem.url) {
      window.location.href = el.item.dataItem.url;
    }
  }

  const fitContent = (el) => {
    setTimeout(() => {
      // console.log('el',el);

      // console.log('delay',main, main.getBBox().height);
      // const content = el.element.querySelector('.dx-scrollview-content');
      // const height = content.offsetHeight
      const dxiMain = el.element.querySelector('.dxdi-main');
      // console.log(dxiMain.transform.baseVal.getItem(0).matrix.e);
      dxiMain.transform.baseVal.getItem(0).setTranslate(dxiMain.transform.baseVal.getItem(0).matrix.e, 10);
      // console.log(dxiMain.getBBox().height);
      // console.log(dxiMain.viewportElement.clientHeight);
      const height = dxiMain.getBBox().height * 1.15

      el.element.style.height = `${height}px`;
      el.element.style.minHeight = `${height}px`;
      const scrollable = el.element.querySelector('.dx-scrollable-container');
      scrollable.style.overflowX = "hidden";
      scrollable.style.overflowY = "hidden";

      // console.log(container);

      // const main = el.element.querySelector('.dx-scrollable-content');

    }, 200);
  }

  return (<div>
    <Diagram readOnly={true} showGrid={false} simpleView={true} onItemClick={navigate} autoZoomMode="fitWidth" onContentReady={fitContent}>
      <Nodes dataSource={flowNodesDataSource} idExpr="id" typeExpr="type" textExpr="text" leftExpr="left" topExpr="top" heightExpr="height" widthExpr="width" styleExpr={style} textStyleExpr={styleText}>
        <AutoLayout type="auto"/>
      </Nodes>
      <Edges dataSource={flowEdgesDataSource} idExpr="id" textExpr="text" fromExpr="fromId" toExpr="toId" toPointIndexExpr="toPointIndex" fromPointIndexExpr="fromPointIndex"/>
      <ViewToolbar visible={false}/>
      <ContextToolbox enable={false}/>
    </Diagram>
    <Helmet>
      <link rel="stylesheet" type="text/css" href="https://cdn3.devexpress.com/jslib/21.2.5/css/dx.common.css"/>
      <link rel="stylesheet" type="text/css" href="https://cdn3.devexpress.com/jslib/21.2.5/css/dx.light.css"/>
      <link rel="stylesheet" type="text/css" href="https://cdn3.devexpress.com/jslib/21.2.5/css/dx-diagram.css"/>
      <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"/>
    </Helmet>
  </div>)
}
export default Diag
