import React, {useRef, useEffect} from 'react';
import Diagram, {
  Nodes,
  Edges,
  AutoLayout,
  Toolbox,
  Group,
  MainToolbar,
  ViewToolbar,
  HistoryToolbar,
  PropertiesPanel,
  Editing,
  GridSize,
  ContextToolbox
} from 'devextreme-react/diagram';
import ArrayStore from 'devextreme/data/array_store';
import {Helmet} from "react-helmet";

const Diag = () => {
  const flowNodes = [
    {
      id: 12,
      text: `Interconnexions des alternatives en pays nantais`,
      url: './ian',
      left: 1,
      top: 2.125,
      kind: "chantier"
    }, {
      id: 14,
      text: 'Ateliers Citoyens',
      left: 8,
      top: 0.625,
      kind: "chantier"
    }, {
      id: 15,
      text: 'Barometre',
      left: 8,
      top: 3.625,
      kind: "chantier"
    }, {
      id: 50,
      text: `Interconnexions et Climat`,
      url: './Campagne',
      top: 0,
      left: 2.5,
      height: 5,
      width: 5,
      type: "ellipse",
      kind: "campagne",
      // childKeys: [ 20 ]
    }, {
      id: 20,
      top: 2,
      left: 3,
      width: 1,
      height: 1,
      type: "ellipse",
      text: 'renforcer les réseau de Alternatives'
    }, {
      id: 21,
      top: 0.5,
      left: 5.5,
      width: 1,
      height: 1,
      type: "ellipse",
      text: 'co-construire les politiques publique'
    }, {
      id: 22,
      top: 3.5,
      left: 5.5,
      width: 1,
      height: 1,
      type: "ellipse",
      text: `mesurer l'impact des politiques publiques`
    }
  ];
  const flowEdges = [
    {
      id: 106,
      fromId: 12,
      toId: 20,
      toLineEnd: "arrow"
    }, {
      id: 107,
      fromId: 14,
      toId: 21,
      toLineEnd: "arrow"
    }, {
      id: 108,
      fromId: 15,
      toId: 22,
      toLineEnd: "arrow"
    }, {
      id: 109,
      fromId: 20,
      toId: 21,
      type: "straight",
      toLineEnd: "outlinedTriangle",
      fromLineEnd: "outlinedTriangle",
      fromPointIndex: 0
    }, {
      id: 110,
      fromId: 22,
      toId: 21,
      toLineEnd: "outlinedTriangle",
      type: "straight"
    }, {
      id: 111,
      fromId: 22,
      toId: 20,
      type: "straight",
      toLineEnd: "outlinedTriangle",
      toPointIndex: 2
    }
  ];

  const flowNodesDataSource = new ArrayStore({key: 'id', data: flowNodes});
  const flowEdgesDataSource = new ArrayStore({key: 'id', data: flowEdges});

  const style = (el) => {
    let style = {};
    switch (el.kind) {
      case "collectif":
        style = {
          ...style,
          fill: "mediumseagreen"
        }
        break;
      case "chantier":
        style = {
          ...style,
          fill: "silver"
        }
        break;
      case "campagne":
        style = {
          ...style,
          fill: "teal"
        }
        break;
      default:

    }
    // console.log('style',style);
    return style;
  }

  const styleText = (el) => {
    // console.log('ALLLLO');
    let style = {};
    switch (el.kind) {
      case "campagne":
        style = {
          ...style,
          "fill": "white"
        }
        break;
      default:
    }
    return style;
  }

  const navigate = (el) => {
    if (el.item.dataItem.url) {
      window.location.href = el.item.dataItem.url;
    }
  }

  const fitContent = (el) => {
    setTimeout(() => {
      // console.log('el',el);

      // console.log('delay',main, main.getBBox().height);
      // const content = el.element.querySelector('.dx-scrollview-content');
      // const height = content.offsetHeight
      const dxiMain = el.element.querySelector('.dxdi-main');
      // console.log(dxiMain.transform.baseVal.getItem(0).matrix.e);
      dxiMain.transform.baseVal.getItem(0).setTranslate(dxiMain.transform.baseVal.getItem(0).matrix.e, 10);
      // console.log(dxiMain.getBBox().height);
      // console.log(dxiMain.viewportElement.clientHeight);
      const height = dxiMain.getBBox().height * 1.15

      el.element.style.height = `${height}px`;
      el.element.style.minHeight = `${height}px`;
      const scrollable = el.element.querySelector('.dx-scrollable-container');
      scrollable.style.overflowX = "hidden";
      scrollable.style.overflowY = "hidden";

      // console.log(container);

      // const main = el.element.querySelector('.dx-scrollable-content');

    }, 200);
  }

  // useEffect(() => {
  //   const main = document.querySelector('dxdi-main');
  //   console.log(main);
  // },[]);

  return (<div>
    <Diagram readOnly={true} showGrid={false} simpleView={true} onItemClick={navigate} autoZoomMode="fitWidth" onContentReady={fitContent}>
      <Nodes dataSource={flowNodesDataSource} idExpr="id" typeExpr="type" textExpr="text" leftExpr="left" topExpr="top" heightExpr="height" widthExpr="width" styleExpr={style} textStyleExpr={styleText}>
        <AutoLayout type="auto"/>
      </Nodes>
      <Edges dataSource={flowEdgesDataSource} idExpr="id" textExpr="text" fromExpr="fromId" toExpr="toId" toPointIndexExpr="toPointIndex" fromPointIndexExpr="fromPointIndex" lineTypeExpr="type" toLineEndExpr="toLineEnd" fromLineEndExpr="fromLineEnd"/>
      <ViewToolbar visible={false}/>
      <ContextToolbox enable={false}/>
    </Diagram>
    <Helmet>
      <link rel="stylesheet" type="text/css" href="https://cdn3.devexpress.com/jslib/21.2.5/css/dx.common.css"/>
      <link rel="stylesheet" type="text/css" href="https://cdn3.devexpress.com/jslib/21.2.5/css/dx.light.css"/>
      <link rel="stylesheet" type="text/css" href="https://cdn3.devexpress.com/jslib/21.2.5/css/dx-diagram.css"/>
      <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"/>
    </Helmet>
  </div>)
}
export default Diag
